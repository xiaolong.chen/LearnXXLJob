package com.chenjl.springcloud.job;

import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import com.xxl.job.core.util.ShardingUtil;
import com.xxl.job.core.util.ShardingUtil.ShardingVO;

import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Service;
/**
 * 任务Handler示例
 * 2018-6-11 16:34:12
 * @author chenjinlong
 */
@Slf4j
@Service
@JobHandler(value="simpleJobHandler")
public class SimpleJobHandler extends IJobHandler {

	@Override
	public ReturnT<String> execute(String param) throws Exception {
		Thread.sleep(10*1000);
		
		ShardingVO shardingVO = ShardingUtil.getShardingVo();
		log.info("SimpleJobHandler execute .... param : [{}],当前分片序号 : [{}], 总分片数 : [{}]",
				param,shardingVO.getIndex(), shardingVO.getTotal());
		return SUCCESS;
	}
}