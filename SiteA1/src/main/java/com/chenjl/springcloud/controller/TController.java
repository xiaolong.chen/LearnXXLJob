package com.chenjl.springcloud.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;
/**
 * 配置Controller
 * 2018-5-24 18:22:57
 * @author chenjinlong
 */
@Slf4j
@RestController
@RequestMapping("/controller/")
public class TController {
	
	@Value("${server.port}")
	private int serverPort;
	@Value("${spring.application.name}")
	private String appName;
	
	
	@ResponseBody
	@RequestMapping(value="t1",method={RequestMethod.POST,RequestMethod.GET})
	public String t1(HttpServletRequest httpServletRequest) {
		log.info("TController.t1 #### appName : {}, serverPort : {}",appName,serverPort);
		return appName+" | "+new Date().toString();
	}
}