package com.chenjl.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;
/**
 * 测试站点A1
 * 2018-6-11 16:14:05
 * @author chenjinlong
 */
@ImportResource(locations="classpath:applicationContext.xml")
@SpringBootApplication
public class SiteA1Application {
	
	public static void main(String[] args) {
		 SpringApplication.run(SiteA1Application.class, args);
	}
}