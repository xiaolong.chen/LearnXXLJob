# LearnXXLJob

**[XXL-Job](https://github.com/xuxueli/xxl-job "XXL-Job")是一个非常不错的Job调度框架，思路很赞；**
<br />
从GitHub上Down一份代码，研究了一下，花了几张简单的图；
<br/><br/>
[公众号Blog](https://mp.weixin.qq.com/s/DwWLRwTgjqBLl74vRGIVTQ "公众号Blog")
<br /><br /><br /><br />


**整体部署图**
![整体部署图](https://gitee.com/xiaolong.chen/LearnXXLJob/raw/master/zpictures/%E6%95%B4%E4%BD%93%E9%83%A8%E7%BD%B2%E5%9B%BE.png)

**部署补充图1**
![部署补充图1](https://gitee.com/xiaolong.chen/LearnXXLJob/raw/master/zpictures/%E6%95%B4%E4%BD%93%E9%83%A8%E7%BD%B2%E5%9B%BE-%E8%A1%A5%E5%85%851.png)

**部署补充图2**
![部署补充图2](https://gitee.com/xiaolong.chen/LearnXXLJob/raw/master/zpictures/%E6%95%B4%E4%BD%93%E9%83%A8%E7%BD%B2%E5%9B%BE-%E8%A1%A5%E5%85%852.png)

**核心流程**
![XXL-Job核心流程](https://gitee.com/xiaolong.chen/LearnXXLJob/raw/master/zpictures/Job%E8%B0%83%E5%BA%A6%E6%B5%81%E7%A8%8B%E5%9B%BE.png)


**核心表ER图**
![核心表ER图](https://gitee.com/xiaolong.chen/LearnXXLJob/raw/master/zpictures/%E6%A0%B8%E5%BF%83%E8%A1%A8ER%E5%9B%BE.png)


**Job分片**
![Job分片](https://gitee.com/xiaolong.chen/LearnXXLJob/raw/master/zpictures/Job%E5%88%86%E7%89%87%E5%B9%BF%E6%92%AD.png)


