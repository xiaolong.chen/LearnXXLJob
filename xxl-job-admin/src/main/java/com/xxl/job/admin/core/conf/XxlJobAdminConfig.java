package com.xxl.job.admin.core.conf;

import java.math.BigInteger;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;
/**
 * xxl-job config
 * @author xuxueli 2017-04-28
 */
@Configuration
@Getter
public class XxlJobAdminConfig implements InitializingBean{
    private static XxlJobAdminConfig adminConfig = null;
    public static XxlJobAdminConfig getAdminConfig() {
        return adminConfig;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        // login token
        String tokenTmp = DigestUtils.md5Hex(loginUsername + "_" + loginPassword);
        LOGIN_IDENTITY_TOKEN = new BigInteger(1, tokenTmp.getBytes()).toString(16);
		
		 adminConfig = this;
    }

    /**
     * 校验登录态
     */
    private String LOGIN_IDENTITY_TOKEN;
    
    @Value("${xxl.job.mail.host}")
    private String mailHost;

    @Value("${xxl.job.mail.port}")
    private String mailPort;

    @Value("${xxl.job.mail.username}")
    private String mailUsername;

    @Value("${xxl.job.mail.password}")
    private String mailPassword;

    @Value("${xxl.job.mail.sendNick}")
    private String mailSendNick;

    @Value("${xxl.job.login.username}")
    private String loginUsername;

    @Value("${xxl.job.login.password}")
    private String loginPassword;

    @Value("${xxl.job.i18n}")
    private String i18n;
}