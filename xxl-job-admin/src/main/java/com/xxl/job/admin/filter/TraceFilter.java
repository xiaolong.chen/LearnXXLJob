package com.xxl.job.admin.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import lombok.extern.slf4j.Slf4j;
/**
 * 跟踪调用记录filter
 * 2018-6-29 15:34:24
 * @author chenjinlong
 */
@Slf4j
public class TraceFilter implements Filter {
	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		log.info("TracerFilter init~");
	}
	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
		HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
		String servletPath = httpServletRequest.getServletPath();
		if(!servletPath.contains("adminlte") && !servletPath.contains("static") ) {
			log.info("跟踪TracerFilter.doFilter,  访问路径 : [{}]",servletPath);
		}
		
		filterChain.doFilter(servletRequest,servletResponse);
	}
	@Override
	public void destroy() {
		log.info("TracerFilter destroy~");
	}
}