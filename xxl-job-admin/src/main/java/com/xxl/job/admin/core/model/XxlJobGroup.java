package com.xxl.job.admin.core.model;

import org.apache.commons.lang3.StringUtils;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
/**
 * job实例, job分组=执行器
 * 
 * xxl_job_qrtz_trigger_group 表 ，job分组=执行器
 * Created by xuxueli on 16/9/30.
 */
@Setter
@Getter
@ToString
public class XxlJobGroup {
    private int id;
    private String appName;
    private String title;
    private int order;
    private int addressType;    // 执行器地址类型：0=自动注册、1=手动录入
    private String addressList;    // 执行器地址列表，多地址逗号分隔(手动录入)

    
    // registry list
    //private List<String> registryList;  // 执行器地址列表(系统注册)
    public List<String> getRegistryNodes() {
        if (StringUtils.isNotBlank(addressList)) {
            return new ArrayList<String>(Arrays.asList(addressList.split(",")));
        }
        return new ArrayList<String>();
    }
}