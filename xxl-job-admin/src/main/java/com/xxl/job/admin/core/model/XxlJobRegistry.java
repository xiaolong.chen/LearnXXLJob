package com.xxl.job.admin.core.model;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
/**
 * 执行器登记信息
 * Created by xuxueli on 16/9/30.
 */
@Setter
@Getter
@ToString
public class XxlJobRegistry {

    private int id;
    
    //executor or admin
    private String registryGroup;
    //xxl_job_qrtz_trigger_group - appName，执行器的name
    private String registryKey;
    //对应执行点的地址
    private String registryValue;
    
    //记录心跳的时间
    private Date updateTime;
}