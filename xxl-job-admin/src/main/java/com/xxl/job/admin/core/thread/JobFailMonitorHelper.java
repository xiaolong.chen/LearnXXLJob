package com.xxl.job.admin.core.thread;

import com.xxl.job.admin.core.model.XxlJobGroup;
import com.xxl.job.admin.core.model.XxlJobInfo;
import com.xxl.job.admin.core.model.XxlJobLog;
import com.xxl.job.admin.core.schedule.XxlJobDynamicScheduler;
import com.xxl.job.admin.core.util.I18nUtil;
import com.xxl.job.admin.core.util.MailUtil;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.collections4.CollectionUtils;

import java.text.MessageFormat;
import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
/**
 * 将Job调度失败信息放置在Queue后，由守护线程发送警告
 * @author xuxueli 2015-9-1 18:05:56
 */
@Slf4j
public class JobFailMonitorHelper {
	
	private static JobFailMonitorHelper instance = new JobFailMonitorHelper();
	public static JobFailMonitorHelper getInstance(){
		return instance;
	}
	//
	public static void monitor(int jobLogId){
		getInstance().queue.offer(jobLogId);
	}

	// ---------------------- alarm ----------------------
	// email alarm template
	private static final String mailBodyTemplate = "<h5>监控告警明细：</span>" +
			"<table border=\"1\" cellpadding=\"3\" style=\"border-collapse:collapse; width:80%;\" >\n" +
			"   <thead style=\"font-weight: bold;color: #ffffff;background-color: #ff8c00;\" >" +
			"      <tr>\n" +
			"         <td>执行器</td>\n" +
			"         <td>任务ID</td>\n" +
			"         <td>任务描述</td>\n" +
			"         <td>告警类型</td>\n" +
			"      </tr>\n" +
			"   <thead/>\n" +
			"   <tbody>\n" +
			"      <tr>\n" +
			"         <td>{0}</td>\n" +
			"         <td>{1}</td>\n" +
			"         <td>{2}</td>\n" +
			"         <td>调度失败</td>\n" +
			"      </tr>\n" +
			"   <tbody>\n" +
			"</table>";

	
	
	
	// ---------------------- monitor ----------------------
	private LinkedBlockingQueue<Integer> queue = new LinkedBlockingQueue<Integer>(0xfff8);

	private Thread monitorThread;
	private volatile boolean toStop = false;
	public void start() {
		monitorThread = new Thread(new Runnable() {
			@Override
			public void run() {
				// monitor
				while (!toStop) {
					try {
						List<Integer> jobLogIdList = new ArrayList<Integer>();
						//一次性从BlockingQueue获取所有可用的数据对象（还可以指定获取数据的个数），通过该方法，可以提升获取数据效率；不需要多次分批加锁或释放锁
						int drainToNum = JobFailMonitorHelper.instance.queue.drainTo(jobLogIdList);
						if(jobLogIdList.size()>0) {
							log.info("从BlockingQueue获取待发送告警任务数量: {},drainToNum:{}",jobLogIdList.size(),drainToNum);
						}
						
						if (CollectionUtils.isNotEmpty(jobLogIdList)) {
							for (Integer jobLogId : jobLogIdList) {
								if (jobLogId==null || jobLogId==0) {
									continue;
								}
								XxlJobLog xxlJobLog = XxlJobDynamicScheduler.xxlJobLogDao.load(jobLogId);
								if (xxlJobLog == null) {
									continue;
								}
								if (IJobHandler.SUCCESS.getCode() == xxlJobLog.getTriggerCode() && xxlJobLog.getHandleCode() == 0) {
									JobFailMonitorHelper.monitor(jobLogId);
									log.info(">>>>>>>>>>> job monitor, job running, JobLogId:{}", jobLogId);
								} 
								else if (IJobHandler.SUCCESS.getCode() == xxlJobLog.getHandleCode()) {
									// job success, pass
									log.info(">>>>>>>>>>> job monitor, job success, JobLogId:{}", jobLogId);
								}
								else if (IJobHandler.FAIL.getCode() == xxlJobLog.getTriggerCode()
										|| IJobHandler.FAIL.getCode() == xxlJobLog.getHandleCode()
										|| IJobHandler.FAIL_RETRY.getCode() == xxlJobLog.getHandleCode() ) {
									// job fail,
									failAlarm(xxlJobLog);
									log.info(">>>>>>>>>>> job monitor, job fail, JobLogId:{}", jobLogId);
								}
								else {
									JobFailMonitorHelper.monitor(jobLogId);
									log.info(">>>>>>>>>>> job monitor, job status unknown, JobLogId:{}", jobLogId);
								}
							}
						}

						TimeUnit.SECONDS.sleep(10);
					} catch (Exception e) {
						log.error("job monitor error:{}", e);
					}
				}

				// monitor all clear
				List<Integer> jobLogIdList = new ArrayList<Integer>();
				int drainToNum = getInstance().queue.drainTo(jobLogIdList);
				log.info("从LinkedBlockingQueue获取任务数量: {},drainToNum:{}",jobLogIdList.size(),drainToNum);
				
				if (jobLogIdList!=null && jobLogIdList.size()>0) {
					for (Integer jobLogId: jobLogIdList) {
						XxlJobLog xxlJobLog = XxlJobDynamicScheduler.xxlJobLogDao.load(jobLogId);
						if (ReturnT.FAIL_CODE == xxlJobLog.getTriggerCode()|| ReturnT.FAIL_CODE==xxlJobLog.getHandleCode()) {
							// job fail,
							failAlarm(xxlJobLog);
							log.info(">>>>>>>>>>> job monitor last, job fail, JobLogId:{}", jobLogId);
						}
					}
				}

			}
		});
		monitorThread.setDaemon(true);
		monitorThread.start();
	}

	public void toStop(){
		toStop = true;
		// interrupt and wait
		monitorThread.interrupt();
		try {
			monitorThread.join();
		} catch (InterruptedException e) {
			log.error(e.getMessage(), e);
		}
	}
	
	/**
	 * fail alarm
	 * @param jobLog
	 */
	private void failAlarm(XxlJobLog xxlJobLog){
		// send monitor email
		XxlJobInfo info = XxlJobDynamicScheduler.xxlJobInfoDao.loadById(xxlJobLog.getJobId());
		if (info!=null && info.getAlarmEmail()!=null && info.getAlarmEmail().trim().length()>0) {

			Set<String> emailSet = new HashSet<String>(Arrays.asList(info.getAlarmEmail().split(",")));
			for (String email: emailSet) {
				XxlJobGroup group = XxlJobDynamicScheduler.xxlJobGroupDao.load(Integer.valueOf(info.getJobGroup()));

				String title = I18nUtil.getString("jobconf_monitor");
				String content = MessageFormat.format(mailBodyTemplate, group!=null?group.getTitle():"null", info.getId(), info.getJobDesc());

				MailUtil.sendMail(email, title, content);
			}
		}
	}

}