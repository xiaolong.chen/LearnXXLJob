package com.xxl.job.core;
/**
 * 全局变量
 * 2018-6-15 19:17:53
 * @author chenjinlong
 */
public class Constant {
	
	//连接调度中心的相对路径
	public static final String ADMIN_BIZ_MAPPING = "/api";
	
	
	//心跳
    public static final int BEAT_TIMEOUT = 30;
    public static final int DEAD_TIMEOUT = BEAT_TIMEOUT * 3;
}