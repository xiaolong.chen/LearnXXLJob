package com.xxl.job.core.biz.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * job执行后回调
 * Created by xuxueli on 17/3/2.
 */
@ToString
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class HandleCallbackParam implements Serializable {
    private static final long serialVersionUID = 42L;

    private int logId;
    private ReturnT<String> executeResult;
}