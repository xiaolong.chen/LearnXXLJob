package com.xxl.job.core.util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
/**
 * sharding vo
 * @author xuxueli 2017-07-25 21:26:38
 */
public class ShardingUtil {
    private static final ThreadLocal<ShardingVO> contextHolder = new InheritableThreadLocal<ShardingVO>();
    
    public static void setShardingVo(ShardingVO shardingVo){
        contextHolder.set(shardingVo);
    }
    public static ShardingVO getShardingVo(){
        return contextHolder.get();
    }
    public static void removeShardingVo(){
        contextHolder.remove();
    }
    
    
    @Getter
    @Setter
    @ToString
    @AllArgsConstructor
    public static class ShardingVO {
        private int index;// sharding index
        private int total;// sharding total
    }
}