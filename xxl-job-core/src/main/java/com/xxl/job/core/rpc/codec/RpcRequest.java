package com.xxl.job.core.rpc.codec;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
/**
 * request
 * @author xuxueli 2015-10-29 19:39:12
 */
@ToString(exclude={"serverAddress","createMillisTime","accessToken"})
@Setter
@Getter
public class RpcRequest implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String serverAddress;
	private long createMillisTime;
	private String accessToken;

    private String className;
    private String methodName;
    private Class<?>[] parameterTypes;
    private Object[] parameters;
}