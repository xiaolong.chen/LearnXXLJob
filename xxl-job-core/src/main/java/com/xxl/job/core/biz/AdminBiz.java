package com.xxl.job.core.biz;

import com.xxl.job.core.biz.model.HandleCallbackParam;
import com.xxl.job.core.biz.model.RegistryParam;
import com.xxl.job.core.biz.model.ReturnT;
/**
 * 调度中心接收 可调度节点的动作
 * @author xuxueli 2017-07-27 21:52:49
 */
public interface AdminBiz {
	
    /**
     * callback
     * Job执行结束后，回调给调度中心
     * @param handleCallbackParams
     * @return
     */
    public ReturnT<String> callback(HandleCallbackParam handleCallbackParam);

    /**
     * registry 服务节点向 执行器注册中心注册， 30s一次
     * @param registryParam
     * @return
     */
    public ReturnT<String> registry(RegistryParam registryParam);
    /**
     * registry remove
     * @param registryParam
     * @return
     */
    public ReturnT<String> registryRemove(RegistryParam registryParam);
    
    /**
     * trigger job for once， 立即出发一次job
     * @param jobId
     * @return
     */
    public ReturnT<String> triggerJob(int jobId);
}