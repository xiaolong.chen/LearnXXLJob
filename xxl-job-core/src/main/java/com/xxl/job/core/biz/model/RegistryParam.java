package com.xxl.job.core.biz.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
/**
 * 注册事件
 * Created by xuxueli on 2017-05-10 20:22:42
 */
@ToString
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class RegistryParam implements Serializable {
    private static final long serialVersionUID = 42L;

    private String registGroup;
    private String registryKey;
    private String registryValue;
}