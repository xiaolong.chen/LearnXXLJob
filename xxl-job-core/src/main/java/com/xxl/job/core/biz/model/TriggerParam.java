package com.xxl.job.core.biz.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
/**
 * 触发job参数，来自调度中心
 * Created by xuxueli on 16/7/22.
 */
@ToString
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class TriggerParam implements Serializable{
    private static final long serialVersionUID = 42L;

    //来自调度中心
    private int jobId;

    //Spring bean name
    private String executorHandler;
    private String executorParams;
    private String executorBlockStrategy;

    //logId来自调度中心，标记唯一一次调度
    private int logId;
    private long logDateTim;

    private String glueType;
    private String glueSource;
    private long glueUpdatetime;

    private int broadcastIndex;
    private int broadcastTotal;
}