package com.xxl.job.core.biz.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
/**
 * Created by xuxueli on 17/3/23.
 */
@ToString
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class LogResult implements Serializable {
    private static final long serialVersionUID = 42L;


    private int fromLineNum;
    private int toLineNum;
    private String logContent;
    private boolean isEnd;
}