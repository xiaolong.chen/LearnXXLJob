package com.xxl.job.core.rpc.codec;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
/**
 * response
 * @author xuxueli 2015-10-29 19:39:54
 */
@ToString
@Setter
@Getter
public class RpcResponse implements Serializable{
	private static final long serialVersionUID = 1L;
	
    private String error;
    private Object result;

    public boolean isError() {
        return error != null;
    }
}