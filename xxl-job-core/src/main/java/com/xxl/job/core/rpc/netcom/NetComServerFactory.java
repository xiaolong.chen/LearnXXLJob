package com.xxl.job.core.rpc.netcom;

import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.rpc.codec.RpcRequest;
import com.xxl.job.core.rpc.codec.RpcResponse;
import com.xxl.job.core.rpc.netcom.jetty.server.JettyServer;

import lombok.extern.slf4j.Slf4j;

import org.springframework.cglib.reflect.FastClass;
import org.springframework.cglib.reflect.FastMethod;

import java.util.HashMap;
import java.util.Map;
/**
 * netcom init
 * @author xuxueli 2015-10-31 22:54:27
 */
@Slf4j
public class NetComServerFactory  {
	//server start
	private JettyServer server = new JettyServer();
	
	public void start(int port, String ip, String appName) throws Exception {
		server.start(port, ip, appName);
	}
	public void destroy(){
		server.destroy();
	}

	
	
	// ---------------------- server instance ----------------------
	/**
	 * init local rpc service map
	 * 保存本地服务实例
	 */
	private static Map<String, Object> serviceMap = new HashMap<String, Object>();
	private static String accessToken;
	public static void putService(Class<?> interfaceClazz, Object serviceInstance){
		serviceMap.put(interfaceClazz.getName(),serviceInstance);
	}
	public static void setAccessToken(String accessToken) {
		NetComServerFactory.accessToken = accessToken;
	}
	
	public static RpcResponse invokeService(RpcRequest request) {
		String clazzName = request.getClassName();
		//log.info("收到调度中心的调度请求, 服务名: {}",clazzName);
		Object serviceInstance = serviceMap.get(clazzName);

		RpcResponse response = new RpcResponse();

		if (System.currentTimeMillis() - request.getCreateMillisTime() > 180000) {
			response.setResult(new ReturnT<String>(ReturnT.FAIL_CODE, "The timestamp difference between admin and executor exceeds the limit."));
			return response;
		}
		if (accessToken!=null && accessToken.trim().length()>0 && !accessToken.trim().equals(request.getAccessToken())) {
			response.setResult(new ReturnT<String>(ReturnT.FAIL_CODE, "The access token[" + request.getAccessToken() + "] is wrong."));
			return response;
		}

		try {
			Class<?> serviceClass = serviceInstance.getClass();
			String methodName = request.getMethodName();
			Class<?>[] parameterTypes = request.getParameterTypes();
			Object[] parameters = request.getParameters();

			FastClass serviceFastClass = FastClass.create(serviceClass);
			FastMethod serviceFastMethod = serviceFastClass.getMethod(methodName, parameterTypes);

			log.info("反射执行bean, clazzName : {},methodName : {}",clazzName,methodName);
			Object result = serviceFastMethod.invoke(serviceInstance,parameters);

			response.setResult(result);
		} catch (Throwable t) {
			t.printStackTrace();
			response.setError(t.getMessage());
		}
		return response;
	}
}